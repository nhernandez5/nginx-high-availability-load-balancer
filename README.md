# Overview
The job of a load balancer is to efficiently distribute incoming traffic across a group of backend servers known as server pool. A load balancer acts as a “traffic cop” sitting in front of your backend servers, routing client requests across all servers capable of handling those requests in a manner that maximizes speed and ensures that no one server is overworked, which would degrade performance.

This project is a proof of concept showcasing how to load balance *dockerized* applications with [nginx](https://docs.nginx.com/nginx/admin-guide/load-balancer/http-load-balancer/). Specifically, a containerized nginx instance applies the round-robin algorithm to load balance between multiple running instances of the same application.

## Prerequisite

* [Docker](https://docs.docker.com/installation/#installation)

## Running a round-robin Nginx load-balancer
From the `docker` directory, run `docker-compose up --build`. This should spin up four containers--three running an express js app, one running the nginx load balncer. The express app is rather simple, it greets you with the name of the server you are visiting.

```shell
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
e4c1e6e09d2d        round_robin_nginx   "nginx -g 'daemon ..."   3 minutes ago      Up 3 minutes       0.0.0.0:5000->80/tcp     docker_nginx_1
4df210193547        express_app         "docker-entrypoint..."   3 minutes ago      Up 3 minutes       5000/tcp                 docker_alpha_1
1ffb72c74304        express_app         "docker-entrypoint..."   3 minutes ago      Up 3 minutes       5000/tcp                 docker_beta_1
f32f06f635df        express_app         "docker-entrypoint..."   3 minutes ago      Up 3 minutes       5000/tcp                 docker_gamma_1
```

Open a web browser and access http://localhost:5000. If everything went well, we will see a web page displaying something along the lines of `Welcome to server "${name}"!`.
If we hit reload on our web browser a few times, from time to time the value of `name` switches between `alpha`, `beta`, and `gamma`. This is the round-robin load balancing algorithm in action.

# High Availability
Coming soon...
Looking for an open source version of NGINX Plus's [Active Health Checks](https://docs.nginx.com/nginx/admin-guide/load-balancer/http-health-check/#active-health-checks)
