const express = require('express');
const app = express();

const PORT = parseInt(process.env.PORT, 10) || 5000;
const NAME = process.env.NAME || 'unspecified';

app.get('/', (req, res) => {
    res.send(`Welcome to server "${NAME}"!`);
});

app.listen(PORT, () => {
    console.log(`🚀 Server ready at http://localhost:${PORT}`);
});
